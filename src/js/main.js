import $ from 'jquery';
import Person from './persion';
import Education from './education';

$.when($.getJSON('http://localhost:3000/person'), $.ready).done(function([
  data
]) {
  const { name, age, description, educations } = data;
  const person = new Person(name, age, description);

  $('#introduce').text(person.introduce());
  $('#description').text(description);
  $('#educations').html(
    educations
      .map(({ year, title, description }) => {
        const education = new Education(year, title, description);
        return `
          <section>
            <h3 class="year">${education.year}</h3>
            <div class="detail gray-block">
              <h3>${education.title}</h3>
              <p>${education.description}</p>
            </div>
          </section>
        `;
      })
      .join('')
  );
});
